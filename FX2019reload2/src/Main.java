// программа в которой идет переключение между окнами по нажатию кнопки

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
  Stage window;
  Scene sc1, sc2;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
      window = primaryStage;
      Label label1 = new Label("Это пециальная презентация, ты в окне 1");
      Button button1 = new Button("Switch to window 2");
      button1.setOnAction(e -> window.setScene(sc2)); //переключение на окно 2
      VBox layout1 = new VBox(20);
      layout1.getChildren().addAll(label1,button1);
      sc1 = new Scene(layout1,200,200);
      //button 2
      Button button2 = new Button("Switch to window 1");
      button2.setOnAction(e -> window.setScene(sc1)); //переключение на окно 1 из окна 2 по кнопке

      StackPane layout2 = new StackPane();
      layout2.getChildren().add(button2); //назначаем лэйауту кнопку
      sc2 = new Scene(layout2, 600,300);
      window.setScene(sc1);
      window.setTitle("Office K Presentation");
      window.show();

  }


}