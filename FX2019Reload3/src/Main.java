// программа, в которой идет вызов окна, описанного в другом класе с применением блокировки предыдущего окна

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
  Stage window;
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;

    window.setTitle("Office K Presentation");
    button = new Button("zhmak");
    button.setOnAction(e -> AlertBox.display("Это окно, которое описано в классе алертбокс", "Это окно блокирует работу предыдущего, пока не закроется"));
    StackPane layout1 = new StackPane();
    layout1.getChildren().add(button);
    Scene scene = new Scene(layout1,200,200);
    window.setScene(scene);
    window.show();

  }


}
