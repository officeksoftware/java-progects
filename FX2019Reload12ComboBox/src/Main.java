//Программа, которая по выбору итема из комбобокса или по кнопке выводит значение в консоль

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Collection;

public class Main extends Application {

  Stage window;
  Scene scene;
  Button button;
  ComboBox<String> comboBox;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    comboBox = new ComboBox<>();
    comboBox.getItems().addAll("Item1", "Item2", "Item3");
    comboBox.setPromptText("combo header"); //defoltovoe
    comboBox.setOnAction(e -> System.out.println(comboBox.getValue()));

    button = new Button("ZhmaK");
    button.setOnAction(e -> getChoice()); //управление в метод

    //Layout
    VBox layout = new VBox(10);
    layout.setPadding(new Insets(20, 20, 20, 20));
    layout.getChildren().addAll(comboBox,button);


    scene = new Scene(layout, 300, 250);
    window.setScene(scene);
    window.show();
  }
private  void getChoice() {
  System.out.println(comboBox.getValue());
}
}