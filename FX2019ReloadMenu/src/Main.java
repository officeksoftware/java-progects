//пилим менюшку: простую, с разделителями, с метами активности и с переключателями состояния.
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {

  Stage window;
  BorderPane layout;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    Menu fileMenu = new Menu("File");     //элемент меню
    MenuItem newFile = new MenuItem("New"); //создаем объект под экшн
    newFile.setOnAction(e -> System.out.println("create new file")); //что будет при нажатии
    fileMenu.getItems().add(newFile);  //записываем в меню

    MenuItem loadFile = new MenuItem("Load"); //создаем объект под экшн
    loadFile.setOnAction(e -> System.out.println("create new file")); //что будет при нажатии
    loadFile.setDisable(true); //деактивировать
    fileMenu.getItems().add(loadFile);

    fileMenu.getItems().add(new MenuItem("Load"));
    fileMenu.getItems().add(new MenuItem("Save"));
    fileMenu.getItems().add(new MenuItem("Save as"));
    fileMenu.getItems().add(new SeparatorMenuItem()); //полоска отделения пунктов меню
    fileMenu.getItems().add(new MenuItem("Exit"));

    Menu editMenu = new Menu("_Edit"); //Доступ к альту
    editMenu.getItems().add(new MenuItem("cut"));
    editMenu.getItems().add(new MenuItem("copy"));
    editMenu.getItems().add(new MenuItem("paste"));

    Menu helpMenu = new Menu("Help");   //меню с чеками
    CheckMenuItem showLines = new CheckMenuItem("Show line numbers");
    showLines.setOnAction(e -> {
      if(showLines.isSelected())
        System.out.println("display nunbers");
      else
        System.out.println("Disabled");
    });
    CheckMenuItem autoSave = new CheckMenuItem("Enable autosave");
    autoSave.setSelected(true);
    helpMenu.getItems().addAll(showLines, autoSave);

    Menu hardLevel = new Menu("Hard level"); //меню радиобаттон
    ToggleGroup hardToggle = new ToggleGroup(); //контейнер
    RadioMenuItem easy = new RadioMenuItem("Easy"); //параметры
    RadioMenuItem med = new RadioMenuItem("Norm");
    RadioMenuItem hard = new RadioMenuItem("HARDCORE");
    easy.setToggleGroup(hardToggle);  //добавление в контейнер
    med.setToggleGroup(hardToggle);
    hard.setToggleGroup(hardToggle);
    hardLevel.getItems().addAll(easy,med,hard); //упаковка в меню

    TextField element = new TextField("ololo");
    Button button = new Button("OK");

    MenuBar bar = new MenuBar();  //контейнер бар для всех менюшек
    bar.getMenus().addAll(fileMenu, editMenu, helpMenu, hardLevel);   //упаковка в бар
    VBox layout2 = new VBox();
    layout2.getChildren().addAll(element, button);
    layout = new BorderPane();
    layout.setTop(bar);

    Scene scene = new Scene(layout, 400, 250);
    window.setScene(scene);
    window.show();
  }
}
