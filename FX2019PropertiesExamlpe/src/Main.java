
import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Main extends Application {
  Stage window;
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    TextField usrtxt = new TextField();
    usrtxt.setMaxWidth(200);
    Label lbl1 = new Label("Hello,");
    Label lbl2 = new Label();
    HBox bottomtxt = new HBox(lbl1,lbl2);
    bottomtxt.setAlignment(Pos.CENTER);

    button = new Button("Закрыть");

    VBox vbox = new VBox(10, usrtxt, bottomtxt);
    vbox.setAlignment(Pos.CENTER);
    lbl2.textProperty().bind(usrtxt.textProperty());

    Scene scene = new Scene(vbox,300,200);
    window.setScene(scene);
    window.show();

  }


}