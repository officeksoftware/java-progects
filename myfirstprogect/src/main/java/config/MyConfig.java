package config;

import net.portfoliobox.officek.entities.Cat;
import net.portfoliobox.officek.entities.Dog;
import net.portfoliobox.officek.entities.Parrot;

@Configuration
public class MyConfig {
    @Bean
    public Cat getCat() {
        return new Cat();
    }

    @Bean("dog")
    public Dog getDog() {
        return new Dog();
    }

    @Bean("parrot-kesha")
    public Parrot getParrot() {
        return new Parrot();
    }
}
