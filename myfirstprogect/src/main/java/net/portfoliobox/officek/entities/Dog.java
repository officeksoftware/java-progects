package net.portfoliobox.officek.entities;

import org.springframework.util.StreamUtils;

public class Dog {
    private String name;
    private int age;
    private String type;

    public Dog(String name, String type, int age) {
        this.name = name;
        this.type = type;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public String getType(){
        return type;
    }
    public int getAge(){
        return age;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAge(int age) {
        this.age = age;
    }
}


