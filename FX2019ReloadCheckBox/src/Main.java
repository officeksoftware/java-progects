//программа, в которой делаем серию чекбоксов, отправляющих по нажатию в консоль определенные сообщения

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.CheckBox;

import java.util.Collection;

public class Main extends Application {

  Stage window;
  Scene scene;
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    //CheckBox
    CheckBox box1 = new CheckBox("LOL");    //объекты чекБокс пишутся с большой Б, иначе это АВТ а не фХ
    CheckBox box2 = new CheckBox("KEK");
    CheckBox box3 = new CheckBox("Cheburek");
    box3.setSelected(true);   //занчение по умолчанию

    button = new Button("ZhmaK");
    button.setOnAction(e -> handleOptions(box1,box2,box3)); //управление в метод

    //Layout
    VBox layout = new VBox(10);
    layout.setPadding(new Insets(20,20,20,20));
    layout.getChildren().addAll(box1, box2, box3, button);


    scene = new Scene(layout, 300, 250);
    window.setScene(scene);
    window.show();
  }

  private void handleOptions(CheckBox box1, CheckBox box2, CheckBox box3) {
    String message = "HEHEEH! ";      //пустое сообщение
    if (box1.isSelected())
      message += "kek, cheburek";     //если выбрано, то добавляется сообщение
    if (box2.isSelected())
      message += "lol, cheburek";
    if (box3.isSelected())
      message += "bez lola i keka ne bivaet chebureka";
    System.out.println(message);
  }
}
