// программа, в которой по кнопке выполняется внешний метод, вызываемый по сетонэкшену
//программа, в которой выполняется лоическое действие по закрытию окна.
//прогамма, вызывающая диалогвое окно на закрытие программы
//программа, зывывающая диалог по кнопке закрытия окна крестик

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
  Stage window;
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");
    window.setOnCloseRequest(e -> { // что делаем по закрытию окна через крестик
      e.consume();    // стопорнуть процесс, потому что этой команде насрать на метод, она рабоатет на закрытие в люом случае
      closeProgram();
    });       //что будет происходить по закрытии окна по крестику
    button = new Button("Закрыть");
    button.setOnAction(e -> closeProgram());
    StackPane layout1 = new StackPane();
    layout1.getChildren().add(button);
    Scene scene = new Scene(layout1,300,200);
    window.setScene(scene);
    window.show();

  }
  private void closeProgram() {
     // System.out.println("file is save"); на случай простого закрытия окна
     //  window.close();
    Boolean answer = ConfirmBox.display("Закрытие окна", "Логическое условие");
    if(answer)
      window.close();
  }

}