import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;

public class ConfirmBox {

  static boolean answer;

  public static boolean display(String title, String message) { // поставлено логическое значение метода
    Stage window = new Stage();
    window.initModality(Modality.APPLICATION_MODAL); //блокировка других окон во время исполнения окна
    window.setTitle(title);
    window.setMinWidth(250);
    window.setMinHeight(200);

    Label label = new Label();
    label.setText(message);

    //Делаем окно диалога с двумя вариантами ответа по кнопкам. В конструкторе уже слито логическое условие.
    Button yesB = new Button("Выдать true");
    Button noB = new Button("Выдать false");

    yesB.setOnAction(e -> {   //по нажатию ДА
      answer=true;            //Вопрос - тру
      window.close();         //Закрыть окно
    });
    noB.setOnAction(e -> {
      answer=false;
      window.close();
    });

    VBox layout = new VBox(10);
    layout.getChildren().addAll(label, yesB, noB);
    layout.setAlignment(Pos.CENTER);
    Scene scene = new Scene(layout);
    window.setScene(scene);
    window.showAndWait();
    return answer;
  }
}