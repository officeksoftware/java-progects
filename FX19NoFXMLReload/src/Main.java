import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.stage.Stage;

public class Main extends Application {
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    primaryStage.setTitle("Office K Software");
    button = new Button();
    button.setText("Жмак");
        //объява анонимного внутреннего класса, который наледуется из-под Event Handler прямо через setonaction
    button.setOnAction(e -> System.out.println("вызов через лямбда выражение"));

    StackPane layout = new StackPane();
    layout.getChildren().add(button);

    Scene scene = new Scene(layout, 200,250);
    primaryStage.setScene(scene);
    primaryStage.show();
  }


}

