
import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import javafx.scene.control.CheckBox;

import java.util.Collection;

public class Main extends Application {

  Stage window;
  Scene scene;
  Button button;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    ChoiceBox<String> choiceBox = new ChoiceBox<>();

    choiceBox.getItems().add("item1");
    choiceBox.getItems().add("item2");
    choiceBox.getItems().addAll("item3", "item4", "item5");
    choiceBox.setValue("item1"); //defoltovoe
//добавлекн слушатель событий - он не позволяет сделать повторный выбор по нажатию, пока не измениттся
    choiceBox.getSelectionModel().selectedItemProperty().addListener((v, oldValue, newValue) -> System.out.println(newValue));


    button = new Button("ZhmaK");
    button.setOnAction(e -> getChoice(choiceBox)); //управление в метод

    //Layout
    VBox layout = new VBox(10);
    layout.setPadding(new Insets(20, 20, 20, 20));
    layout.getChildren().addAll(choiceBox,button);


    scene = new Scene(layout, 300, 250);
    window.setScene(scene);
    window.show();
  }

  private void getChoice(ChoiceBox<String> choiceBox) {
    String food = choiceBox.getValue();
    System.out.println(food+" ");

  }
}
