//пилим таблицу с редактурой и удалением
import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.HBox;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Collection;

public class Main extends Application {

  Stage window;
  Scene scene;
  TableView<ProdTable> tablo;   //Объява в конструкторе
  TextField nameInput, priceInput, qInput;




  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");
//колонки
    TableColumn<ProdTable, String> nameColumn = new TableColumn<>("Name");
    nameColumn.setMaxWidth(200);
    nameColumn.setCellValueFactory(new PropertyValueFactory<>("name"));

    TableColumn<ProdTable, Double> priceColumn = new TableColumn<>("Price");
    priceColumn.setMaxWidth(100);
    priceColumn.setCellValueFactory(new PropertyValueFactory<>("price"));

    TableColumn<ProdTable, Double> qColumn = new TableColumn<>("Quantity");
    qColumn.setMaxWidth(100);
    qColumn.setCellValueFactory(new PropertyValueFactory<>("quantity"));

    //инпуты
    nameInput = new TextField();
    nameInput.setPromptText("Name");
    nameInput.setMinWidth(100);

    priceInput = new TextField();
    priceInput.setPromptText("Price");
    priceInput.setMinWidth(100);

    qInput = new TextField();   //поля ввода
    qInput.setPromptText("Quantity");
    qInput.setMinWidth(100);

    Button addB = new Button("Add");    //кнопки добавления
    addB.setOnAction(e -> addNewItem());
    Button delB = new Button("Delete");
    delB.setOnAction(e -> deleteItem());



    tablo = new TableView<>();
    tablo.setItems(getProduct());  //ссылаемся на метод, инициализирующий объекты таблицы
    tablo.getColumns().addAll(nameColumn,priceColumn,qColumn);
    HBox hbox = new HBox();   //нижняя панель
    hbox.setPadding(new Insets(10,10,10,10));
    hbox.setSpacing(10);
    hbox.getChildren().addAll(nameInput, priceInput, qInput, addB, delB);
    //Layout
    VBox layout = new VBox();
    layout.getChildren().addAll(tablo, hbox);

    scene = new Scene(layout, 300, 250);
    window.setScene(scene);
    window.show();
  }

  //метод события добавления элемента

  public void addNewItem() {
    ProdTable itemTable = new ProdTable();  //создаем новый итем
    itemTable.setName(nameInput.getText()); //брать все из текстовых полей
    itemTable.setPrice(Double.parseDouble(priceInput.getText()));
    itemTable.setQuantity(Integer.parseInt(qInput.getText()));
    tablo.getItems().add(itemTable);  //добаить новый элемент в таблицу
    nameInput.clear();  //очистить поля
    priceInput.clear();
    qInput.clear();

  }
  public void deleteItem() {  //метод удаления элементов
    ObservableList<ProdTable> prodSelect, allSelect;
    allSelect = tablo.getItems();   //выбор всего
    prodSelect = tablo.getSelectionModel().getSelectedItems();
    prodSelect.forEach(allSelect::remove);    //удалить элементы

  }

  public ObservableList<ProdTable> getProduct() {
    //построкове объявление элементов, описание которых написано в классе продТэйбл
    ObservableList<ProdTable> products = FXCollections.observableArrayList();
    products.add(new ProdTable("Macbook", 699.90, 10));
    products.add(new ProdTable("Ipad", 399.90, 50));
    products.add(new ProdTable("Iphone 11", 1999.90, 1000));
    products.add(new ProdTable("Sasung lopata es 11", 1599.90, 2322));
    products.add(new ProdTable("YoPtaPhone", 299.90, 100));
    products.add(new ProdTable("IpoTs", 69.90, 1));
    return products;

  }

}
