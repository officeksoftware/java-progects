package sample.Interfaces.impls;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import sample.Interfaces.bookengine;
import sample.Objects.Contact;

import java.util.ArrayList;


public class CollecionContacts implements bookengine {

  private ObservableList<Contact> contactList = FXCollections.observableArrayList();
  @Override
  public void add (Contact person) {
    contactList.add(person);
  }
  @Override
  public void update (Contact person) {

  }
  @Override
  public void del (Contact person) {
    contactList.remove(person);
  }

  public ObservableList<Contact> getContactList() {
    return contactList;
  }


}
