package sample.Interfaces;
import sample.Objects.Contact;

public interface bookengine {
  void add(Contact person);

  void update(Contact person);

  void del(Contact person);
}