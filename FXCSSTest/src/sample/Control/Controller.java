package sample.Control;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class Controller {
  public void showDialog(ActionEvent actionEvent) {
    try {
      Stage stage = new Stage();
      Parent root = FXMLLoader.load(getClass().getResource("fxml/redactor.fxml"));
      stage.setTitle("Редактор");
      stage.setMaxHeight(579);
      stage.setMaxWidth(309);
      stage.setResizable(false);
      stage.setScene(new Scene(root));
      stage.initModality(Modality.WINDOW_MODAL);
      stage.initOwner(((Node)actionEvent.getSource()).getScene().getWindow());
      stage.show();
    }
    catch (IOException e) {
      e.printStackTrace();
    }

  }
}
