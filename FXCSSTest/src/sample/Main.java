package sample;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import sample.Interfaces.impls.CollecionContacts;
import sample.Objects.Contact;
import sample.Interfaces.impls.CollecionContacts;


public class Main extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("fxml/sample.fxml"));
        primaryStage.setTitle("Hello World");
        primaryStage.setScene(new Scene(root, 300, 275));
        primaryStage.show();
    }

    private void testData() {
        CollecionContacts contacts = new CollecionContacts();
        Contact person = new Contact();
        person.setName("ivan");
        person.setPhone("123123");

        Contact person2 = new Contact();
        person.setName("fedor");
        person.setPhone("123124");

    }

    public static void main(String[] args) {
        launch(args);
    }
}
