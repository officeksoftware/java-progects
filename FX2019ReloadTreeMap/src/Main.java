import javafx.application.Application;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.Collection;

public class Main extends Application {

  Stage window;
  Scene scene;
  Button button;
  TreeView<String> tree;

  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");


    TreeItem<String> root, branch1, branch2; //задаем элементы дерева
    //ставим пути
    root = new TreeItem<>();  //контейнер для ключа
    root.setExpanded(true);
    branch1 = makeBranch("branch1", root);
    makeBranch("item1", branch1);
    makeBranch("item2", branch1);
    makeBranch("item3", branch1);
    branch2 = makeBranch("branch2", root); //делаем ветвь, в кавчках отобрадаемое имя.
    makeBranch("item4", branch2);
    makeBranch("item5", branch2);
    //create tree
    tree = new TreeView<>(root);
    tree.setShowRoot(false);
    tree.getSelectionModel().selectedItemProperty()   //слушатель события - при выборе будет вывод в консоль
            .addListener((v, oldVal, newVal)-> {
              if(newVal !=null)
                System.out.println(newVal.getValue());
            });



    //Layout
    StackPane layout = new StackPane();

    layout.getChildren().add(tree);

    scene = new Scene(layout, 300, 250);
    window.setScene(scene);
    window.show();
  }
    public TreeItem<String> makeBranch(String title, TreeItem<String> parent) {
    TreeItem<String> item = new TreeItem<>(title);
    item.setExpanded(true);
    parent.getChildren().add(item);
    return item;


  }
}