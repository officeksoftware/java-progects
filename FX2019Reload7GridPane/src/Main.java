//программа, в которой делаем окошко логина пароля без ввода данных. Просто окно

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.*;
import javafx.stage.Stage;

public class Main extends Application {
  Stage window;


  public static void main(String[] args) {
    launch(args);
  }

  @Override
  public void start(Stage primaryStage) throws Exception {
    window = primaryStage;
    window.setTitle("Office K Presentation");

    GridPane gridPane = new GridPane();
    gridPane.setPadding(new Insets(10,10,10,10)); //задаем зону неприкосновенности
    gridPane.setVgap(8);
    gridPane.setHgap(10);

    Label nameLabel = new Label("Username:");
    GridPane.setConstraints(nameLabel, 0,0);

    TextField txtfld = new TextField("user");
    GridPane.setConstraints(txtfld,1,0);

    Label psLabel = new Label("Password:");
    GridPane.setConstraints(psLabel, 0,1);

    TextField txtfld2 = new TextField();
    txtfld2.setPromptText("password");
    GridPane.setConstraints(txtfld2, 1,1);

    Button btn1 = new Button("Login");
    GridPane.setConstraints(btn1, 1,2);

    gridPane.getChildren().addAll(nameLabel, txtfld, psLabel, txtfld2, btn1);


    Scene scene = new Scene(gridPane,300,200);
    window.setScene(scene);
    window.show();

  }

}